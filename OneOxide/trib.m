function erg=trib(E,Vd)
%Transfer matrix, constant effective mass
%energy normalized to eV, mass to m0, hbar to 1 => use x_normalized=x/x0
e0=1.60217646e-19; hbar=1.05457168e-34; me=9.10938188e-31;
ml=1; mr=1;
global V_Ti; global mb; global W_Ti; global W_Au;
%V_Ti=0.1; mb=20; %0.1..1eV, 2..25
%W_Ti=4.33; W_Au=5.0; %W_Ti=3.95..4.33 eV; W_Au=4.7..5.2 eV
%EF_Au=5.51; EF_Ti=11.63; V_Au=4.2; V_Ti=2.8; mb=0.38 %V_Ti=3.2;; %in eV,me; Gunther
EF_Au=5.51; EF_Ti=8.54; V_Au=V_Ti+(W_Au-W_Ti); %V_Ti=3.2; %in eV,me
db=2.2; %1.982; %in nm

if(Vd>0) Vl=-EF_Au; Vbl=V_Au; Vbr=V_Ti-Vd; Vr=-Vd-EF_Ti;
else Vl=-EF_Au+Vd; Vbl=V_Au+Vd; Vbr=V_Ti; Vr=-EF_Ti;
end;

mL=ml*me; mR=mr*me; n=100; d=db*1e-9; E=E*e0; VL=Vl*e0; VR=Vr*e0; VbL=Vbl*e0; VbR=Vbr*e0;

z=(0:(n-1))/(n-1)*d; V=VbL+(VbR-VbL)/d*z;
meff=mb*me*(z>=0);
k=sqrt(2*meff.*(E-V))/hbar; D=z(2)-z(1);
kL=sqrt(2*mL*(E-VL))/hbar; kR=sqrt(2*mR*(E-VR))/hbar; bL=kL/mL; bR=kR/mR;

b1=kL/mL; b2=k(1)/meff(1); T2=1/(2*b2)*[b2+b1 b2-b1;b2-b1 b2+b1];
for m=1:(n-1)
        k1=k(m); k2=k(m+1); b1=k(m)/meff(m); b2=k(m+1)/meff(m+1);
        if(b2~=0) T0=[0.5*(1+b1/b2)*exp(i*(k1+k2)*D/2) 0.5*(1-b1/b2)*exp(i*(k2-k1)*D/2);0.5*(1-b1/b2)*exp(i*(k1-k2)*D/2) 0.5*(1+b1/b2)*exp(-i*(k1+k2)*D/2)];
        else T0=[1 0; 0 1]; end;
        T2=T0*T2;
end;
b1=k(n)/meff(n); b2=kR/mR; T2=1/(2*b2)*[b2+b1 b2-b1;b2-b1 b2+b1]*T2;

erg=bL/bR/abs(T2(1,1))^2;
