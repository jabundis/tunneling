function erg=Tsu(Vd)
%energy normalized to eV, mass to m0, hbar to 1 => use x_normalized=x/x0
%with x0=hbar/sqrt(1eV*m0)=2.7604e-10m
e0=1.60217646e-19; hbar=1.05457168e-34; me=9.10938188e-31; kB=1.3806503e-23;

T=300;
kT=kB*T/e0;
for k=1:16000 E(k)=k*0.001-4.0003; T(k)=trib(E(k),Vd);
    if(mod(k,100)==0)
        %disp(E(k));
    end;
end;
if(Vd>0) J=trapz(E*e0,T.*log((1+exp(-E/kT))./(1+exp(-(E+Vd)/kT))));
else J=trapz(E*e0,T.*log((1+exp(-(E-Vd)/kT))./(1+exp(-E/kT))));
end;
J=e0*me*(kT*e0)/2/pi^2/hbar^3*J;
erg=J/10000; %to get A/cm^2
