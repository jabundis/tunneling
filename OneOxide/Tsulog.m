function erg=Tsulog(par,Vdv)
%energy normalized to eV, mass to m0, hbar to 1 => use x_normalized=x/x0
%with x0=hbar/sqrt(1eV*m0)=2.7604e-10m
%V=-5:0.25:5; global W_Au; W_Au=5; Jlog=Tsulog([7.1966e-001  2.1291e+000  3.9500e+000],V);
e0=1.60217646e-19; hbar=1.05457168e-34; me=9.10938188e-31; kB=1.3806503e-23;
global V_Ti; global mb; global W_Ti;
V_Ti=par(1); mb=par(2); W_Ti=par(3);
T=300; Jn=[];
kT=kB*T/e0;
n=0;
for Vd=Vdv
    n=n+1;
for k=1:16000 E(k)=k*0.001-4.0003; T(k)=trib(E(k),Vd);
    if(mod(k,100)==0)
        %disp(E(k));
    end;
end;
if(Vd>0) J=trapz(E*e0,T.*log((1+exp(-E/kT))./(1+exp(-(E+Vd)/kT))));
else J=trapz(E*e0,T.*log((1+exp(-(E-Vd)/kT))./(1+exp(-E/kT))));
end;
J=e0*me*(kT*e0)/2/pi^2/hbar^3*J;
Jn(n)=J;
end;
disp(par);
disp(log(abs(Jn/10000)));
erg=log(abs(Jn/10000)); %to get A/cm^2
